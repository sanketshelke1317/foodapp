﻿using FoodDelivery.Exceptions;
using FoodDelivery.Interfaces;
using FoodDelivery.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Repository
{
    internal class CartRepository:CartInterface
    {
        List<Cart> cartItems;

        public CartRepository()
        {
            cartItems = new List<Cart>();
        }

        public void AddItemToCart(Cart cart)
        {
            cartItems.Add(cart);
            Console.WriteLine($"{cart.foodItem.Name} Added to Cart");
        }

        internal List<Cart> GetAllCartItems()
        {
            if (!cartItems.Any())
            {
                throw (new CartIsEmpty("Cart is Empty"));
            }
            else return cartItems;
        }

        public double TotalAmount()
        {
            double total = 0;
            foreach(Cart cart in cartItems)
            {
                total += cart.foodItem.Price*cart.Quantity;
            }
            return total;
        }

        internal void DeleteAllCartItems()
        {
            cartItems.Clear();
        }

        internal void EditItemFromCart(int itemNumber)
        {
            Console.WriteLine(cartItems[itemNumber]);
            Console.WriteLine("Enter 1 to Edit Quantity or 0 to Delete");

            if (Convert.ToInt32(Console.ReadLine()) == 1)
            {
                Console.WriteLine("Enter quantity");
                cartItems[itemNumber].Quantity = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Cart Updated");
            }
            else
            {
                cartItems.RemoveAt(itemNumber);
                Console.WriteLine($"{cartItems[itemNumber].foodItem.Name} Removed form Cart");
            }
        }
    }
}
