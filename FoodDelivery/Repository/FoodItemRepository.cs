﻿using FoodDelivery.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Repository
{
    internal class FoodItemRepository
    {
        List<FoodItem> foodItems;

        public FoodItemRepository()
        {
            foodItems = new List<FoodItem>()
            {
                new FoodItem(){ Id = 1, Name = "Veg Fried Rice", Category = "Veg", Price = 100},
                new FoodItem(){ Id = 2, Name = "Rice", Category = "Veg", Price = 100},
                new FoodItem(){ Id = 3, Name = "Fried Rice", Category = "NonVeg", Price = 100},
                new FoodItem(){ Id = 4, Name = "Red Chilly", Category = "NonVeg", Price = 100},
            };
        }

        public List<FoodItem> GetFoodItems()
        {
            return foodItems;
        }
    }
}
