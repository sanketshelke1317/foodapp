﻿using FoodDelivery.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Interfaces
{
    internal interface CartInterface
    {
        public void AddItemToCart(Cart cart);
        public double TotalAmount();
    }
}
