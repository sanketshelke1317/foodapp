﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Exceptions
{
    internal class CartIsEmpty:ApplicationException
    {
        public CartIsEmpty()
        {

        }
        public CartIsEmpty(string message):base(message)
        {

        }
    }
}
