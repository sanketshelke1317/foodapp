﻿using FoodDelivery.Exceptions;
using FoodDelivery.Model;
using FoodDelivery.Repository;


CartRepository cartRepository = new CartRepository();
#region checkregion
string[] availableLocations = { "Mumbai", "Pune", "Kolkata" };

Console.WriteLine("Enter Your Name");
string userName = Console.ReadLine();
Console.WriteLine("Enter Location");
string userLocation = Console.ReadLine();

if (availableLocations.Contains(userLocation,StringComparer.OrdinalIgnoreCase)){
    Console.WriteLine("Delivery Available");
    START:
    Console.WriteLine("===================================");
    Console.WriteLine("Select Option: ");

    foreach (int i in Enum.GetValues(typeof(Menu)))
    {
        Console.WriteLine($"{i} : {Enum.GetName(typeof(Menu),i)}");
    }

    try
    {

        Console.WriteLine("Enter Value: ");

        switch (Convert.ToInt32(Console.ReadLine()))
        {

            #region menu
            case 0:
                FoodItemRepository foodItemRepository = new FoodItemRepository();

                Console.WriteLine("1) Veg \n0) NonVeg");

                if (Convert.ToInt32(Console.ReadLine()) == 1)
                {

                    var vegItems = foodItemRepository.GetFoodItems().Where(x => x.Category == "Veg");
                    foreach(var vegItem in vegItems)
                    {
                        Console.WriteLine(vegItem);
                    }
                }
                else
                {
                    var nonVegItems = foodItemRepository.GetFoodItems().Where(x => x.Category == "NonVeg");
                    foreach (var nonVegItem in nonVegItems)
                    {
                        Console.WriteLine(nonVegItem);
                    }
                }

                List<FoodItem> foodItems = foodItemRepository.GetFoodItems();
                Console.WriteLine("Enter ID of Item to be added to the Cart");
                FoodItem foodItem1 = foodItems[Convert.ToInt32(Console.ReadLine()) - 1];

                Console.WriteLine("Enter Quantity");
                Cart cart = new Cart() { foodItem = foodItem1, Quantity = Convert.ToInt32(Console.ReadLine()) };
                cartRepository.AddItemToCart(cart);

                #region buynow
                Console.WriteLine("1) BuyNow \n0) Menu");

                if (Convert.ToInt32(Console.ReadLine()) == 1)
                {
                    goto case 3;
                }
                else goto START;
            #endregion

            #endregion
            #region veiwCart
            case 1:
                List<Cart> cartItems = cartRepository.GetAllCartItems();
                foreach (Cart cart1 in cartItems)
                {
                    Console.WriteLine(cart1);
                }

                Console.WriteLine($"Total Amount : {cartRepository.TotalAmount()}");
                goto START;
            #endregion
            #region updateCart
            case 2:
                List<Cart> cartItems3 = cartRepository.GetAllCartItems();
                int i = 0;
                foreach (Cart cart1 in cartItems3)
                {
                    Console.WriteLine($" ({i++}): {cart1}");
                }

                Console.WriteLine("Enter Id of Item to edit");

                cartRepository.EditItemFromCart(Convert.ToInt32(Console.ReadLine()));




                goto case 2;
            #endregion
            #region bill
            case 3:
                Console.WriteLine(userName);
                List<Cart> cartItems2 = cartRepository.GetAllCartItems();
                foreach (Cart cart1 in cartItems2)
                {
                    Console.WriteLine(cart1);
                }
                DateTime dateTime= DateTime.Now;
                Console.WriteLine($"Total Amount : {cartRepository.TotalAmount()*0.18 + cartRepository.TotalAmount()} with 18% gst\n{dateTime}");

                Console.WriteLine("Enter 1 to Buy or 0 to exit");

                if (Convert.ToInt32(Console.ReadLine()) == 1)
                {

                    cartRepository.DeleteAllCartItems();
                    Console.WriteLine("OrderPlaced");

                }

                goto START;
            #endregion
            #region exit
            case 4:
                break;
            default:
                Console.WriteLine("Invalid Value");
                goto START;

                #endregion



        }
    }
    catch(ArgumentOutOfRangeException)
    {
        Console.WriteLine("Invalid Input");
        goto START;
    }
    catch (CartIsEmpty ex)
    {
        Console.WriteLine(ex.Message);
        goto START;
    }

}
else
{
    Console.WriteLine($"Delivery not available at {userLocation}");
}
#endregion