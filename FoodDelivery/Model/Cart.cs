﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Model
{
    internal class Cart
    {
        public int Quantity { get; set; }
        public FoodItem foodItem { get; set; }

        public override string ToString()
        {
            return $"Name : {foodItem.Name} \t Quantity : {Quantity}";
        }
    }
    

}
