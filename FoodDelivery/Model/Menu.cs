﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Model
{
    enum Menu
    {
        View_Menu,
        View_Cart,
        Update_Cart,
        Pay_Bill_From_Cart,
        Exit
    }
}
